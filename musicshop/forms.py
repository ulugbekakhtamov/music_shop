from django import forms
from django.contrib.auth import get_user_model

User = get_user_model()

class LoginForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'логин'
        self.fields['password'].label = 'пароль'

    def clean(self):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        user = User.objects.filter(username=username).first
        if not  user:
            raise forms.ValidationError(f"Пользователь с логином {username} не найден")
        if not user.check_password(password):
            raise forms.ValidationError(f"Неверный пароль")
        return self.cleaned_data

class RegistrationForm(forms.ModelForm):

    confirm_password = forms.CharField(widget=forms.PasswordInput)
    password = forms.CharField(widget=forms.PasswordInput)
    phone = forms.CharField(required=False)
    address = forms.CharField(required=False)
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].label = 'логин'
        self.fields['confirm_password'].label = 'подвердите пароль'
        self.fields['password'].label = 'пароль'
        self.fields['phone'].label = 'телефон номер'
        self.fields['email'].label = 'почта'
        self.fields['first_name'].label = 'имя'
        self.fields['last_name'].label = 'фамилия'

    def clean_email(self):
         email = self.cleaned_data['email']
         domain = email.split('.')[-1]
         if domain in ['net', 'xyz']:
             raise forms.ValidationError(f"Регистратсия для домена {domain} невозможно")
         if User.objects.filter(email=email).exists():
             raise forms.ValidationError(f"Данный почтовый адрес уже зарегистирован")
         return email

    def clean_username(self):

        username = self.cleaned_data['username']
        if User.objects.filter(username = username).exists():
            raise forms.ValidationError(f"Имя {username} занята. Попробуте другое")
        return username

    def clean(self):
        password = self.cleaned_data['password']
        confirm_password = self.cleaned_data['confirm_password']
        if password != confirm_password:
            raise forms.ValidationError("Пароли не совпадают")
        return self.cleaned_data

    class Meta:
        model = User
        fields = ['username', 'password', 'confirm_password', 'first_name', 'last_name', 'address', 'phone', 'email' ]